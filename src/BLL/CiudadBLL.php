<?php

namespace App\BLL;

use App\Entity\Ciudad;

class CiudadBLL extends BaseBLL
{
    public function nuevo(array $data)
    {
        $ciudad = new Ciudad();
        $ciudad->setNombre($data['nombre']);

        return $this->guardaValidando($ciudad);
    }

    public function toArray($ciudad)
    {
        if ( is_null ($ciudad))
            return null;
        if (!($ciudad instanceof Ciudad))
            throw new Exception("La entidad no es un Contacto");

        return [
            'id' => $ciudad->getId(),
            'nombre' => $ciudad->getNombre()
        ];
    }
}