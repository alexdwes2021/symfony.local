<?php

namespace App\BLL;

use App\Entity\Ciudad;
use App\Entity\Contacto;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ContactoBLL extends BaseBLL
{
    private function guardaImagen($request, $contacto, $data) {
        $arr_foto = explode (',', $data['foto']);
        if ( count ($arr_foto) < 2)
            throw new BadRequestHttpException('formato de imagen incorrecto');

        $imgFoto = base64_decode ($arr_foto[1]);
        if (!is_null($imgFoto))
        {
            $fileName = $data['nombreFoto'] . '-'. time() . '.jpg';
            $contacto->setFoto($fileName);
            $ifp = fopen ($this->fotosDirectory . '/' . $fileName, "wb");
            if ($ifp)
            {
                $ok = fwrite ($ifp, $imgFoto);

                fclose ($ifp);

                if ($ok)
                    return $this->guardaValidando($contacto);
            }
        }

        throw new \Exception('No se ha podido cargar la imagen del contacto');
    }

    private function actualizaContacto(Request $request, Contacto $contacto, array $data)
    {
        $ciudad = $this->em->getRepository(Ciudad::class)->find($data['ciudad']);

        $contacto->setNombre($data['nombre']);
        $contacto->setTelefono($data['telefono']);
        $contacto->setCiudad($ciudad);

        return $this->guardaImagen($request, $contacto, $data);
    }

    public function nuevo(Request $request, array $data)
    {
        $contacto = new Contacto();
        return $this->actualizaContacto($request, $contacto, $data);
    }

    public function getContactosFiltrados(
        string $order, string $ciudad=null, string $nombre=null, string $telefono=null) : array
    {
        $contactos = $this->em->getRepository(Contacto:: class )->getContactosFiltrados(
            $order, $ciudad, $nombre, $telefono);

        return $this->entitiesToArray($contactos);
    }

    public function update(Request $request, Contacto $contacto, array $data)
    {
        return $this->actualizaContacto($request, $contacto, $data);
    }

    public function toArray($contacto)
    {
        if ( is_null ($contacto))
            return null;
        if (!($contacto instanceof Contacto))
            throw new Exception("La entidad no es un Contacto");

        return [
            'id' => $contacto->getId(),
            'nombre' => $contacto->getNombre(),
            'ciudad' => $contacto->getCiudad()->getNombre(),
            'telefono' => $contacto->getTelefono(),
        ];
    }
}