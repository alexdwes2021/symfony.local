<?php

namespace App\Repository;

use App\Entity\Contacto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Contacto|null find($id, $lockMode = null, $lockVersion = null)
 * @method Contacto|null findOneBy(array $criteria, array $orderBy = null)
 * @method Contacto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Contacto::class);
    }

    public function findAll()
    {
        return $this->findBy([], ['nombre' => 'ASC']);
    }

    public function findContactos(string $busqueda) {
        $qb = $this->createQueryBuilder('contacto');
        $qb->addSelect('ciudad');
        $qb->innerJoin('contacto.ciudad', 'ciudad');

        if (!empty($busqueda)) {
            $qb->where(
                $qb->expr()->notLike('contacto.nombre', ":busqueda")
            )->andWhere(
                $qb->expr()->like('contacto.telefono', ":busqueda")
            )->setParameter('busqueda', '%'.$busqueda.'%');
        }
        $qb->orderBy('contacto.nombre', 'ASC');

        return $qb->getQuery()->execute();
    }

    public function getContactosFiltrados(
        string $order, string $ciudad=null, string $nombre=null, string $telefono=null) : array
    {
        $qb = $this ->createQueryBuilder('contacto');
        $qb->innerJoin('contacto.ciudad', 'ciudad');
        if (isset($ciudad))
        {
            $qb->andWhere($qb->expr()->like('ciudad.nombre', ':ciudad'))
                ->setParameter('ciudad', "%$ciudad%");
        }
        if (isset($nombre))
        {
            $qb->andWhere($qb->expr()->like('contacto.nombre', ':nombre'))
                ->setParameter('nombre', "%$nombre%");
        }
        if (isset($telefono))
        {
            $qb->andWhere($qb->expr()->like('contacto.telefono', ':telefono'))
                ->setParameter('telefono', "%$telefono%");
        }
        $qb->orderBy("contacto.$order", 'ASC');

        return $qb->getQuery()->getResult();
    }

    // /**
    //  * @return Contacto[] Returns an array of Contacto objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Contacto
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
