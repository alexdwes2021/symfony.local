<?php

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PagesController extends AbstractController
{
    /**
     * @Route(
     *     "/inicio/{id}",
     *     name="dwes_inicio",
     *     requirements={"id"="\d+"}
     * )
     */
    public function inicio(LoggerInterface $logger, int $id=3)
    {
        $number = mt_rand(0, 100);

        $logger->info("Se ha generado el número $number");

        return $this->render('inicio.html.twig', [
            'number' => $id
        ]);
    }
}