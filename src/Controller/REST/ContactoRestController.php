<?php

namespace App\Controller\REST;

use App\BLL\ContactoBLL;
use App\Entity\Contacto;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactoRestController extends BaseApiController
{
    /**
     * @Route("/contactos.{_format}",
     *     name="get_contactos",
     *     defaults={"_format": "json"},
     *     requirements={"_format": "json"},
     *     methods={"GET"}
     * )
     * @Route("/contactos/ordenados/{order}", name="get_contactos_ordenados")
     */
    public function getAll(Request $request, ContactoBLL $contactoBLL, string $order='id')
    {
        $ciudad = $request->query->get('ciudad');
        $nombre = $request->query->get('nombre');
        $telefono = $request->query->get('telefono');

        $contactos = $contactoBLL->getContactosFiltrados($order, $ciudad, $nombre, $telefono);

        return $this->getResponse($contactos);
    }

    /**
     * @Route("/contactos/{id}.{_format}",
     *     name="get_contacto",
     *     requirements={"id": "\d+", "_format": "json"},
     *     defaults={"_format": "json"},
     *     methods={"GET"}
     * )
     */
    public function getOne(Contacto $contacto, ContactoBLL $contactoBLL)
    {
        return $this->getResponse($contactoBLL->toArray($contacto));
    }

    /**
     * @Route("/contactos.{_format}", name="post_contactos",
     *      defaults={"_format": "json"},
     *      requirements={"_format": "json"},
     *     methods={"POST"}
     * )
     */
    public function post(Request $request, ContactoBLL $contactoBLL)
    {
        $data = $this->getContent($request);
        $contacto = $contactoBLL->nuevo($request, $data);
        return $this->getResponse(
            $contacto, Response:: HTTP_CREATED
        );
    }

    /**
     * @Route("/contactos/{id}.{_format}",
     *     name="update_contacto",
     *     requirements={"id": "\d+", "_format": "json"},
     *     defaults={"_format": "json"},
     *     methods={"PUT"}
     * )
     */
    public function update(Request $request, Contacto $contacto, ContactoBLL $contactoBLL)
    {
        $data = $this->getContent($request);

        $contacto = $contactoBLL->update($request, $contacto, $data);

        return $this->getResponse($contacto);
    }

    /**
     * @Route("/contactos/{id}.{_format}",
     *     name="delete_contacto",
     *     requirements={"id": "\d+", "_format": "json"},
     *     defaults={"_format": "json"},
     *     methods={"DELETE"}
     * )
     */
    public function delete(Contacto $contacto, ContactoBLL $contactoBLL)
    {
        $contactoBLL->delete($contacto);
        return $this->getResponse(null, Response:: HTTP_NO_CONTENT );
    }
}