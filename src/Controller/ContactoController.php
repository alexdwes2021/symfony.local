<?php

namespace App\Controller;

use App\Entity\Ciudad;
use App\Entity\Contacto;
use App\Form\ContactoForm;
use App\Helper\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ContactoController extends AbstractController
{
    /**
     * @Route(
     *     "/contactos",
     *     name="dwes_contactos",
     *     methods={"GET","POST"}
     * )
     */
    public function listar(Request $request)
    {
        $busqueda = '';
        $contactoRepository = $this->getDoctrine()->getRepository(Contacto::class);
        if ($request->getMethod() === 'POST')
            $busqueda = $request->request->get('busqueda');

        $contactos = $contactoRepository->findContactos($busqueda);

        return $this->render('Contacto/index.html.twig', [
            'contactos' => $contactos
        ]);
    }

    /**
     * @Route(
     *     "/contactos/{id}",
     *     name="dwes_ver_detalle_contacto",
     *     requirements={"id"="\d+"},
     *     methods={"GET"}
     * )
     */
    public function verDetalle(Contacto $contacto)
    {
        return $this->render('Contacto/detalle.html.twig', [
            'contacto' => $contacto
        ]);
    }

    /**
     * @Route(
     *     "/contactos/{id}/delete",
     *     name="dwes_eliminar_contacto",
     *     requirements={"id"="\d+"},
     *     methods={"GET"}
     * )
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function eliminar(
        Contacto $contacto, EntityManagerInterface $em)
    {
        try
        {
            throw new BadRequestHttpException("No se puede eliminar el contacto");
            $em->remove($contacto);
            $em->flush();
        }
        catch (\Exception $exception) {
            $this->addFlash('error-eliminar', $exception->getMessage());
        }

        return $this->redirectToRoute('dwes_contactos');
    }

    /**
     * @Route(
     *     "/contactos/new",
     *     name="dwes_nuevo_contacto",
     *     methods={"GET", "POST"}
     * )
     */
    public function nuevo(Request $request, FileUploader $fileUploader)
    {
        $error = null;
        try {
            $contacto = new Contacto();
            $form = $this->createForm(ContactoForm::class, $contacto);

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                // $form->getData() holds the submitted values
                // but, the original `$task` variable has also been updated
                $contacto = $form->getData();
                $fotoFile = $form->get('fotoFile')->getData();

                if ($fotoFile) {
                    // Move the file to the directory where brochures are stored

                    $contacto->setFotoFile($fotoFile);
//                    $newFilename = $fileUploader->upload($fotoFile);
//                    $contacto->setFoto($newFilename);
                }

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($contacto);
                $entityManager->flush();

                return $this->redirectToRoute('dwes_contactos');
            }

        } catch (BadRequestHttpException $e) {
            $error = $e->getMessage();
        }

        return $this->render('Contacto/form-contacto.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route(
     *     "/contactos/{id}/edit",
     *     name="dwes_editar_contacto",
     *     requirements={"id"="\d+"},
     *     methods={"GET", "POST"}
     * )
     */
    public function editar(Request $request, Contacto $contacto, FileUploader $fileUploader)
    {
        $form = $this->createForm(ContactoForm::class, $contacto);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $contacto = $form->getData();
            $fotoFile = $form['fotoFile']->getData();

            if ($fotoFile) {
                $contacto->setFotoFile($fotoFile);
                $contacto->setUpdateAt(new \DateTime());
//                $newFilename = $fileUploader->upload($fotoFile);
//                $contacto->setFoto($newFilename);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contacto);
            $entityManager->flush();

            return $this->redirectToRoute('dwes_contactos');
        }

        return $this->render('Contacto/form-contacto.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}